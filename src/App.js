import React from "react";
import { Button, Grid, Typography, Card } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { increment, decrement } from "./Redux/counterAction"; // Assuming you have defined these action creators

const App = () => {
  const dispatch = useDispatch();
  const counter = useSelector((state) => state.counter);

  const handleIncrement = () => {
    dispatch(increment());
  };

  const handleDecrement = () => {
    dispatch(decrement());
  };

  return (
    <Grid
      container
      alignItems="center"
      justifyContent="center"
      sx={{ padding: "2rem" }}
      direction="column"
    >
      <h1>Counter Redux-React App</h1>
      <Card variant="outlined" sx={{ padding: "1rem" }}>
        <Button variant="contained" color="error" onClick={handleDecrement}>
          Decrement
        </Button>
        <Typography variant="h5" sx={{ margin: "0 1rem", fontWeight: "bold" }}>
          {counter}
        </Typography>
        <Button variant="contained" color="success" onClick={handleIncrement}>
          Increment
        </Button>
      </Card>
    </Grid>
  );
};

export default App;
